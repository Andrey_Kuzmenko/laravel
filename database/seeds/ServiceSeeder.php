<?php

use Illuminate\Database\Seeder;
use App\Models\Service;
use App\Models\Image;
use App\Models\Category;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $count_category = Category::all()->count();
        Service::create(
            [
                'master_id'=>1,
                'image_id'=>(Image::create(['name'=>'makeup-1.jpg']))->id,
                'category_id'=>rand(1,$count_category),
                'name'=>'Наращивание ресниц',
                'note'=>'Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. Numquam, totam.',
                'price_min'=>null,
                'price_max'=>28.40,
                'time'=>861
            ]);
        Service::create(
            [
                'master_id'=>1,
                'image_id'=>(Image::create(['name'=>'makeup-2.jpg']))->id,
                'category_id'=>rand(1,$count_category),
                'name'=>'Стрижка',
                'note'=>'Consectetur adipisicing elit.',
                'price_min'=>22.40,
                'price_max'=>null,
                'time'=>37
            ]);
        Service::create(
            [
                'master_id'=>1,
                'image_id'=>(Image::create(['name'=>'makeup-3.jpg']))->id,
                'category_id'=>rand(1,$count_category),
                'name'=>'Массаж',
                'note'=>'Numquam, totam. Consectetur adipisicing elit.',
                'price_min'=>22.40,
                'price_max'=>22.40,
                'time'=>65344
            ]);
        Service::create(
            [
                'master_id'=>1,
                'image_id'=>(Image::create(['name'=>'makeup-4.jpg']))->id,
                'category_id'=>rand(1,$count_category),
                'name'=>'Чистка лица',
                'note'=>'Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. ',
                'price_min'=>null,
                'price_max'=>28.40,
                'time'=>651
            ]);
        Service::create(
            [
                'master_id'=>1,
                'image_id'=>(Image::create(['name'=>'makeup-5.jpg']))->id,
                'category_id'=>rand(1,$count_category),
                'name'=>'Чистка желудка',
                'note'=>'Consectetur adipisicing elit.',
                'price_min'=>22.40,
                'price_max'=>null,
                'time'=>37
            ]);
        Service::create(
            [
                'master_id'=>1,
                'image_id'=>(Image::create(['name'=>'makeup-6.jpg']))->id,
                'category_id'=>rand(1,$count_category),
                'name'=>'Массаж шеи',
                'note'=>'Numquam, totam. Consectetur adipisicing elit.',
                'price_min'=>22.40,
                'price_max'=>22.40,
                'time'=>157
            ]);
        Service::create(
            [
                'master_id'=>1,
                'image_id'=>(Image::create(['name'=>'makeup-7.jpg']))->id,
                'category_id'=>rand(1,$count_category),
                'name'=>'Наращивание бровей',
                'note'=>'Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. Numquam, totam. Consectetur adipisicing elit. Numquam, totam.',
                'price_min'=>null,
                'price_max'=>28.40,
                'time'=>61
            ]);
        Service::create(
            [
                'master_id'=>1,
                'image_id'=>(Image::create(['name'=>'makeup-3.jpg']))->id,
                'category_id'=>rand(1,$count_category),
                'name'=>'Стрижка бровей',
                'note'=>'Consectetur adipisicing elit.',
                'price_min'=>22.40,
                'price_max'=>null,
                'time'=>15
            ]);
        Service::create(
            [
                'master_id'=>1,
                'image_id'=>(Image::create(['name'=>'makeup-4.jpg']))->id,
                'category_id'=>rand(1,$count_category),
                'name'=>'Массаж спины',
                'note'=>'Numquam, totam. Consectetur adipisicing elit.',
                'price_min'=>22.40,
                'price_max'=>22.40,
                'time'=>159
            ]);
        Service::create(
            [
                'master_id'=>1,
                'image_id'=>(Image::create(['name'=>'makeup-5.jpg']))->id,
                'category_id'=>rand(1,$count_category),
                'name'=>'Услуги косметолога',
                'note'=>'Consectetur a. Numquam, totam.',
                'price_min'=>null,
                'price_max'=>28.40,
                'time'=>61
            ]);
        Service::create(
            [
                'master_id'=>1,
                'image_id'=>(Image::create(['name'=>'makeup-3.jpg']))->id,
                'category_id'=>rand(1,$count_category),
                'name'=>'Маникюр',
                'note'=>'Consectetur adipisicing elit.',
                'price_min'=>22.40,
                'price_max'=>null,
                'time'=>37
            ]);
        Service::create(
            [
                'master_id'=>1,
                'image_id'=>(Image::create(['name'=>'makeup-3.jpg']))->id,
                'category_id'=>rand(1,$count_category),
                'name'=>'Педикюр',
                'note'=>'Numquam, totam. Consectetur adipisicing elit.',
                'price_min'=>22.40,
                'price_max'=>22.40,
                'time'=>157
            ]);


    }
}
