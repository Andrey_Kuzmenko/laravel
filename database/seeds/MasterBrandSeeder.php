<?php

use Illuminate\Database\Seeder;
use App\Models\Master;
use App\Models\Brand;
use App\Models\MasterBrand;
class MasterBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $count_master = Master::all()->count();
        $all_count_brand = Brand::all()->count();

        for($i=1; $i<$count_master; $i++){
            $count_brand = rand(0,20);
                for($j=0; $j<$count_brand; $j++){
                    do{
                        $brand_id = rand(1, $all_count_brand);
                        $master_brands = MasterBrand::where([
                            ['brand_id',$brand_id],
                            ['master_id',$i],
                        ])->first();
                        //dump($master_brands);
                    }while($master_brands);

                    MasterBrand::create([
                        'brand_id'=>$brand_id,
                        'master_id'=>$i
                    ]);
                }
        }
    }
}
