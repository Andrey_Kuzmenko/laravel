<?php

use Illuminate\Database\Seeder;

class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('providers')->insert([
            [
                'name'=>'vkontakte',
                'key'=>'6016422',
                'secret'=>'5hz12WJrpaA7sbTUSV4F',
                'redirect_url'=>'http://cruckie.avrora.team/public/callback_login/vkontakte'
            ],
            [
                'name'=>'google',
                'key'=>'614705047301-80gip78f4ergrr9c89r9vafj0lc669c3.apps.googleusercontent.com',
                'secret'=>'mURp2hAr3gbGR5XuzAUsE-Fw',
                'redirect_url'=>'http://cruckie.avrora.team/public/callback_login/google'
            ],
            [
                'name'=>'facebook',
                'key'=>'1633360363359859',
                'secret'=>'c16e2051d31e6caa5e7b2041ed592c51',
                'redirect_url'=>'http://cruckie.avrora.team/public/callback_login/facebook'
            ]
        ]);

    }
}
