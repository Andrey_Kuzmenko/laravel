<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(ProviderSeeder::class);
        $this->call(ImageSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(MasterSeeder::class);
        $this->call(EmailSeeder::class);
        $this->call(PhoneSeeder::class);
        $this->call(AddressSeeder::class);
        $this->call(SalonSeeder::class);
        $this->call(SalonAddressSeeder::class);
        $this->call(BrandSeeder::class);
        $this->call(MasterBrandSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(SalonMasterSeeder::class);



    }
}
