<?php

use Illuminate\Database\Seeder;
use App\Models\Salon;

class SalonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Salon::create(
            [
                'name'=>"Avrora",
                'user_id'=>2,
                'note'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam blanditiis cumque ducimus esse facilis ipsum neque nobis porro quibusdam, quos sunt veniam. Autem error hic nisi non odio suscipit voluptate!',
                'image_id'=>3,
            ]
        );
        Salon::create(
            [
                'name'=>"AVRORA.TEAM",
                'user_id'=>1,
                'note'=>'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam blanditiis cumque ducimus esse facilis ipsum neque nobis porro quibusdam, quos sunt veniam. Autem error hic nisi non odio suscipit voluptate!',
                'image_id'=>3,
            ]
        );


    }
}


