<?php

use Illuminate\Database\Seeder;
use App\Models\Address;


class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Address::create(
            [
                'longitude'=>47.8429194,
                'latitude'=>35.1385178,
                'country'=> 'Украина',
                'city'=>'Запорожье',
                'street'=>'Независимой Украины, 57А',
            ]);
        Address::create(
            [
                'longitude'=>47.877341,
                'latitude'=>35.020253,
                'country'=> 'Украина',
                'city'=>'Запорожье',
                'street'=>'Зестафонская, 1',
            ]
        );
    }
}
