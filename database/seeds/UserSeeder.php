<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Services\ImageService;
use Faker\Factory;
use App\Models\Image;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create(
            [
                'name'=>'Сергей',
                'surname'=>'Корж',
                'avatar_id'=>1,
                'factor'=>1,
                //'login'=>'qwe@qwe.ru',
                'password'=>bcrypt('qwerty')
            ]
        );
        User::create(
            [
                'name'=>'Лысенко',
                'surname'=>'Вячеслав',
                'avatar_id'=>2,
                'background_id'=>3,
                'role_id'=>3,
                'factor'=>0,
                //'login'=>'t@t.ru',
                'password'=>bcrypt('qwerty')
            ]
        );

        for($i=0; $i<10; $i++){

            $user = factory(User::class)->create();
          /*  $user->avatar_id = $this->createAndSaveImage($user->id);
            $user->background_id = $this->createAndSaveImage($user->id);
            $user->save();*/

        }



    }

    protected function createAndSaveImage($id)
    {
        $faker = Factory::create();
        $img = new ImageService($faker->imageUrl(800,400, 'cats', true, 'Faker'));
        $name = $img->save($id);
        $image =Image::create([
            'name'=>$name,
        ]);

        return $image->id;

    }
}
