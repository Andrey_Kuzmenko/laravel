<?php

use Illuminate\Database\Seeder;
use App\Models\Email;

class EmailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $count = \App\Models\User::all()->count();
        factory(Email::class,3*$count)->create();

        /*Email::create(
            [
                'user_id'=>1,
                'email'=>'sergvetrov2017@yandex.ru',
                'confirm'=>1,
            ]
        );*/
        Email::create(
            [
                'user_id'=>1,
                'email'=>'qwe@qwe.ru',
                'confirm'=>1
            ]
        );
       /* Email::create(
            [
                'user_id'=>2,
                'email'=>'vyacheslavlysen@gmail.com',
                'confirm'=>1,
            ]
        );*/
        Email::create(
            [
                'user_id'=>2,
                'email'=>'t@t.ru',
                'confirm'=>1,
            ]
        );
    }
}
