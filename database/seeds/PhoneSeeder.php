<?php

use Illuminate\Database\Seeder;
use App\Models\Phone;

class PhoneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Phone::create([
            'number'=>'+3290985853353',
            'user_id'=>1,
            'confirm'=>true,
            'primary'=>true,

        ]);
        $count = \App\Models\User::all()->count();
        factory(Phone::class,3*$count)->create();
    }
}
