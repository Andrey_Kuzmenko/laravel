<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Category::create(['name'=>'Стрижка']);
        Category::create(['name'=>'Массаж']);
        Category::create(['name'=>'Макияж']);
        Category::create(['name'=>'Наращивание']);
        Category::create(['name'=>'Маникюр']);
        Category::create(['name'=>'Педюкюр']);

    }
}
