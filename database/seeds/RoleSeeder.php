<?php

use Illuminate\Database\Seeder;

use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Role::create(
            ['name'=>'notVereficated']
        );
        Role::create(
            ['name'=>'Client']
        );
        Role::create(
            ['name'=>'Master']
        );
        Role::create(
            ['name'=>'PremiumMaster']
        );
        Role::create(
            ['name'=>'PremiumSalon']
        );
        Role::create(
            ['name'=>'Admin']
        );

    }
}
