<?php

use Illuminate\Database\Seeder;
use App\Models\SalonAddress;

class SalonAddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        SalonAddress::create(
          [
              'salon_id'=>1,
              'address_id'=>1,
          ]
        );

        SalonAddress::create(
          [
              'salon_id'=>1,
              'address_id'=>2,
          ]
        );
    }
}
