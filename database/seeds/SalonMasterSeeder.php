<?php

use Illuminate\Database\Seeder;
use App\Models\SalonMaster;

class SalonMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
            SalonMaster::create(
                [
                    'salon_id'=>1,
                    'master_id'=>1,
                ]
            );

            SalonMaster::create(
                [
                    'salon_id'=>1,
                    'master_id'=>2,

                ]
            );
            SalonMaster::create(
                [
                    'salon_id'=>1,
                    'master_id'=>3,

                ]
            );
            SalonMaster::create(
                [
                    'salon_id'=>1,
                    'master_id'=>4,

                ]
            );
            SalonMaster::create(
                [
                    'salon_id'=>1,
                    'master_id'=>5,

                ]
            );
            SalonMaster::create(
                [
                    'salon_id'=>2,
                    'master_id'=>4,

                ]
            );
            SalonMaster::create(
                [
                    'salon_id'=>2,
                    'master_id'=>5,

                ]
            );
            SalonMaster::create(
                [
                    'salon_id'=>2,
                    'master_id'=>3,

                ]
            );

    }
}
