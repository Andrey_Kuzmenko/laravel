<?php

use Illuminate\Database\Seeder;
use App\Models\Master;
use App\Models\Image;
use App\Models\MasterImage;
class MasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(Master::class,5)->create();

        for($i=1;$i<11; $i++){
            $img = Image::create(
                [
                    'name'=>'cat'.$i.'.jpg',
                ]
            );
            MasterImage::create(
                [
                    'master_id'=>2,
                    'image_id'=>$img->id,
                    'sort'=>$i
                ]
            );
        }
        for($i=1;$i<11; $i++){
            $img = Image::create(
                [
                    'name'=>'cat'.$i.'.jpg',
                ]
            );
            MasterImage::create(
                [
                    'master_id'=>1,
                    'image_id'=>$img->id,
                    'sort'=>$i
                ]
            );
        }
    }
}
