<?php

use Illuminate\Database\Seeder;
use App\Models\Brand;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Brand::create(['name'=>'Ив Роше']);
        Brand::create(['name'=>'L\'OCCITANE']);
        Brand::create(['name'=>'Body Shop']);
        Brand::create(['name'=>'Natura Siberica']);
        Brand::create(['name'=>'Organic Shop']);
        Brand::create(['name'=>'Рецепты бабушки Агафьи']);
        Brand::create(['name'=>'Weleda']);
        Brand::create(['name'=>'Stella McCartney CARE']);
        Brand::create(['name'=>'GUAM']);
        Brand::create(['name'=>'Lancaster']);
        Brand::create(['name'=>'Мыловаров']);
        Brand::create(['name'=>'AHAVA']);
        Brand::create(['name'=>'Korres']);
        Brand::create(['name'=>'Demeter']);
        Brand::create(['name'=>'Melvita']);
        Brand::create(['name'=>'Teana']);
        Brand::create(['name'=>'Bloom']);
        Brand::create(['name'=>'Dr. Hauschka']);
        Brand::create(['name'=>'Mirra']);
        Brand::create(['name'=>'Himalaya Herbals']);
        Brand::create(['name'=>'Sanoflore']);
        Brand::create(['name'=>'Madara']);
        Brand::create(['name'=>'Savonry']);
        Brand::create(['name'=>'Dzintars']);
        Brand::create(['name'=>'Lavera']);
        Brand::create(['name'=>'TRIND']);
        Brand::create(['name'=>'Logona']);
        Brand::create(['name'=>'БИОбьюти']);
        Brand::create(['name'=>'Bottega Verde']);
        Brand::create(['name'=>'Diptyque']);
        Brand::create(['name'=>'THALGO']);
        Brand::create(['name'=>'KeraSys Homme']);
        Brand::create(['name'=>'Велиния']);
        Brand::create(['name'=>'Aesop']);
        Brand::create(['name'=>'ЗЕЙТУН']);
        Brand::create(['name'=>'Attirance']);
        Brand::create(['name'=>'Холинка']);
        

    }
}
