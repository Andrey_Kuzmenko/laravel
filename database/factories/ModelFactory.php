<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Master;
use App\Models\Email;
use App\Models\Phone;
use App\Models\Role;


/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    $fullname = explode(' ',$faker->name);
    //$count_role = Role::all()-count();
    return [

        'role_id'=>$faker->numberBetween(1,3),
        //'avatar_id'=>$faker->numberBetween(1,100),
        //'background_id'=>$faker->numberBetween(1,100),
        'name' => $fullname[0],
        'surname'=>$fullname[1],
        //'login' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('qwerty'),
        'remember_token' => str_random(60),
        'bdate'=>$faker->date('Y-m-d', $max='now'),
        //'confirm_register'=>$faker->numberBetween(0,1),
    ];
});

$factory->define(Master::class, function (Faker\Generator $faker) {
    static $inc=0;

    return [
        'id'=>++$inc,
        'note'=>$faker->text(500)
    ];
});

$factory->define(Email::class, function (Faker\Generator $faker) {
    $count_users = \App\Models\User::all()->count();
    return [
        'user_id'=>$faker->numberBetween(1,$count_users),
        'email'=>$faker->unique()->safeEmail,
        'confirm'=>$faker->numberBetween(0,1)
    ];
});


$factory->define(Phone::class, function (Faker\Generator $faker) {
    $count_users = \App\Models\User::all()->count();
    return [
        'user_id'=>$faker->numberBetween(1,$count_users),
        'number'=>$faker->e164phoneNumber(),
        'confirm'=>$faker->numberBetween(0,1)
    ];
});

//Нужно сделать через DI ImageService





