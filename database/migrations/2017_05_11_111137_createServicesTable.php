<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('master_id')->notNull();
            $table->unsignedInteger('image_id')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->string('name')->notNull();
            $table->text('note')->nullable()->default(null);
            $table->decimal('price_min')->nullable();
            $table->decimal('price_max')->nullable();
            $table->unsignedInteger('time')->nullable()->default(60*15);
            
            $table->foreign('master_id')
                            ->references('id')
                            ->on('masters')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

            $table->foreign('image_id')
                            ->references('id')
                            ->on('images')
                            ->onDelete('set null')
                            ->onUpdate('cascade');
            $table->foreign('category_id')
                            ->references('id')
                            ->on('categories')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
