<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timetables', function (Blueprint $table) {
            $table->unsignedInteger('salon_master_id')->notNull();
            $table->enum('day_of_week',['Mo','Tu','We', 'Th', 'Fr', 'Sa', 'Su']);
            $table->time('start')->default('00:00:00');
            $table->time('end')->default('00:00:00');

            $table->foreign('salon_master_id')
                            ->references('id')
                            ->on('salon_masters')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timetables');
    }
}
