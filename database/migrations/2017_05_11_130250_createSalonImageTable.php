<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salon_images', function (Blueprint $table) {
            $table->integer('image_id')->unsigned()->notNull();
            $table->integer('salon_id')->unsigned()->notNull();
            $table->unsignedInteger('sort')->notNull();

            $table->primary(['image_id', 'salon_id']);

            $table->foreign('image_id')
                            ->references('id')
                            ->on('images')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

            $table->foreign('salon_id')
                            ->references('id')
                            ->on('salons')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salon_images');
    }
}
