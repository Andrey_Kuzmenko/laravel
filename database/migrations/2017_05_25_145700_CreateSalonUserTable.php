<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salon_users', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->notNull();
            $table->unsignedInteger('salon_id')->notNull();
            $table->primary(['user_id', 'salon_id']);

            $table->foreign('user_id')
                            ->references('id')
                            ->on('users')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');
            $table->foreign('salon_id')
                            ->references('id')
                            ->on('salons')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salon_users');
    }
}
