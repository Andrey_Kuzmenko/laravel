<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salon_addresses', function (Blueprint $table) {
            $table->integer('salon_id')->unsigned()->notNull();
            $table->integer('address_id')->unsigned()->notNull();
            $table->primary(['salon_id', 'address_id']);


            $table->foreign('salon_id')
                            ->references('id')
                            ->on('salons')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

            $table->foreign('address_id')
                            ->references('id')
                            ->on('addresses')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salon_addresses');
    }
}
