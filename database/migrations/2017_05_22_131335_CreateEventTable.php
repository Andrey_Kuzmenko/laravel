<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('service_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('salon_id')->nullable();
            $table->date('date');
            $table->time('start');
            $table->time('end');
            $table->boolean('complete')->default(0);

            $table->foreign('service_id')
                            ->references('id')
                            ->on('services')
                            ->onDelete('set null')
                            ->onUpdate('cascade');
            $table->foreign('user_id')
                            ->references('id')
                            ->on('users')
                            ->onDelete('set null')
                            ->onUpdate('cascade');
            $table->foreign('salon_id')
                            ->references('id')
                            ->on('salons')
                            ->onDelete('set null')
                            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
