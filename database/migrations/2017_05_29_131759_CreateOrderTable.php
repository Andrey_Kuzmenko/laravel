<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id')->nullable();
            $table->decimal('cost');
            $table->boolean('complete')->default(false);
            $table->enum('method',['cart','cash']);
            $table->dateTime('data_time')->nullable()->default(null);


            $table->foreign('event_id')
                            ->references('id')
                            ->on('events')
                            ->onDelete('set null')
                            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
