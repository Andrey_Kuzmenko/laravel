<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_images', function (Blueprint $table) {
            $table->integer('image_id')->unsigned()->notNull();
            $table->integer('master_id')->unsigned()->notNull();
            $table->unsignedInteger('sort')->notNull();

            $table->primary(['image_id', 'master_id']);

            $table->foreign('image_id')
                            ->references('id')
                            ->on('images')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

            $table->foreign('master_id')
                            ->references('id')
                            ->on('masters')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_images');
    }
}
