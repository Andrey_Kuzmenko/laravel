<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterBrandTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_brands', function (Blueprint $table) {
            $table->integer('master_id')->unsigned()->notNull();
            $table->integer('brand_id')->unsigned()->notNull();

            $table->primary(['master_id', 'brand_id']);


            $table->foreign('master_id')
                            ->references('id')
                            ->on('masters')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

            $table->foreign('brand_id')
                            ->references('id')
                            ->on('brands')
                            ->onDelete('cascade')
                            ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_brands');
    }
}
