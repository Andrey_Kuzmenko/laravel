<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalonMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salon_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('master_id')->unsigned()->notNull();
            $table->integer('salon_id')->unsigned()->notNull();

            $table->foreign('master_id')
                ->references('id')
                ->on('masters')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('salon_id')
                ->references('id')
                ->on('salons')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salon_masters');
    }
}
