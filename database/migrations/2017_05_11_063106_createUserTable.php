<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned()->nullable()->default(null);
            $table->integer('avatar_id')->unsigned()->nullable()->default(null);
            $table->integer('background_id')->unsigned()->nullable()->default(null);
            $table->string('name')->nullable()->default(null);
            $table->string('surname')->nullable()->default(null);
            $table->date('bdate')->nullable()->default(null);
            //$table->string('login')->nullable()->default(null);
            $table->char('password',60)->nullable()->default(null);
            $table->char('remember_token',64)->nullable()->default(null);
            //$table->boolean('confirm_register')->default(false);
            $table->boolean('factor')->default(false);



            $table->timestamps();


            $table->foreign('avatar_id')
                            ->references('id')
                            ->on('images')
                            ->onDelete('set null')
                            ->onUpdate('cascade');

            $table->foreign('background_id')
                            ->references('id')
                            ->on('images')
                            ->onDelete('set null')
                            ->onUpdate('cascade');


            $table->foreign('role_id')
                            ->references('id')
                            ->on('roles')
                            ->onDelete('set null')
                            ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
