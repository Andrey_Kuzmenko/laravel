<?php
/**
 * Created by PhpStorm.
 * User: avrora
 * Date: 5/4/17
 * Time: 4:07 PM
 */

namespace App\SocialProfile\Models;

use Socialite;

abstract class AbstractSocialProfile
{
    protected $user;

    protected $fields = [];

    public function __construct()
    {
        $this->initUser();
        $this->setFields();
    }

    public function __get($key)
    {
        if(isset($this->fields[$key])){
            return $this->fields[$key];
        }
        return null;
    }

    abstract protected function initUser();

    protected function setFields()
    {
        if($this->user->name==="")
        {
            $arr_names[]='';
            $arr_names[]='';
        }else{
            $arr_names = explode(" ", $this->user->name);
        }
        $this->fields['id'] = $this->user->user['id'];
        $this->fields['name']=$arr_names[0];
        $this->fields['surname']=$arr_names[1];
        $this->fields['email']=$this->user->email;
        $this->fields['phone']=null;
        $this->fields['bdate']=null;
    }



    public function getData()
    {
        return $this->fields;
    }

}