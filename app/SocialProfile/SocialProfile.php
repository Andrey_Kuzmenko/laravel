<?php

namespace App\SocialProfile;


class SocialProfile
{

    static public function make($driver)
    {
        $driver[0] = strtoupper($driver[0]);
        $nameclass = 'App\SocialProfile\Models\\'.$driver.'SocialProfile';
        return new $nameclass();
    }

}