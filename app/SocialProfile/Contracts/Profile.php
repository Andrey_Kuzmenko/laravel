<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 09.05.2017
 * Time: 14:38
 */

namespace App\SocialProfile\Contracts;


interface Profile
{
    public function getData();
}