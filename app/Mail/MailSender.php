<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailSender extends Mailable
{
    use Queueable, SerializesModels;

    public $event;

    public function __construct($event)
    {
        $this->event = $event;
    }

    public function build()
    {
        return $this->from('kuzmenkophp@gmail.com', 'Крюки')
            ->subject('Ссылка на подтверждение регистрации: ' . $this->event)
            ->view('emails.template');
    }

    /**
     * Build the message.
     *
     * @return $this
     */

}
