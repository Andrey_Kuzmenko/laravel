<?php

namespace App\Social\Google;


class Provider extends \SocialiteProviders\Google\Provider
{

    public function getNormalizeAllFields()
    {
        $user = $this->user();
        return [
            'social_user_id' => $user->id,
            'name' => $user->user['name']['givenName'],
            'surname' => $user->user['name']['familyName'],
            'email'=>$user->email
        ];
    }
}
