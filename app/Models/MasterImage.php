<?php

namespace App\Models;


class MasterImage extends Model
{
    const MAX_COUNT_IMAGE = 20;
    //
    protected $fillable=['master_id', 'image_id', 'sort'];

}
