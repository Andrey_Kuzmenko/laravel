<?php

namespace App\Models;


class Email extends Model
{
    //
    public $primaryKey = 'user_id';
    protected $fillable = ['user_id', 'email', 'confirm'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
