<?php

namespace App\Models;


class Brand extends Model
{
    //
    public function masters()
    {
        return $this->belongsToMany(Master::class);
    }
}
