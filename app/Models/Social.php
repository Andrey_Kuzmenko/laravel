<?php

namespace App\Models;

class Social extends Model
{
    //
    protected $fillable = ['user_id', 'provider_id', 'social_id'];


    public function user()
    {
        return self::belongsTo(User::class);
    }
}
