<?php
/**
 * Created by PhpStorm.
 * User: avrora
 * Date: 5/4/17
 * Time: 4:07 PM
 */

namespace App\Models;
use App\SocialProfile\Contracts\Profile;
use Socialite;

class UserSocialAccount
{
    protected $user;

    protected $fields = [];

    public function __construct($provider_name)
    {

        $this->user = Socialite::driver($provider_name)->fields(
                [
                    'uid',
                    'first_name',
                    'last_name',
                    'screen_name',
                    'photo_max_orig',
                    'bdate',
                    'city',
                    'country',
                    'contacts',
                    'site'
                ])->user();
        $this->setFields();
    }

    public function __get($key)
    {

        if(isset($this->fields[$key])){

            return $this->fields[$key];
        }

        return null;

    }

    protected function setFields()
    {
        $arr_names = explode(" ", $this->user->name);
        $this->fields['social_user_id'] = $this->user->user['id'];
        $this->fields['name']=$arr_names[0];
        $this->fields['surname']=$arr_names[1];
        $this->fields['email']=$this->user->email;
        $this->fields['avatar']=$this->user->user['photo_max_orig'];
    }


    public function getFields()
    {
        return $this->fields;
    }

}