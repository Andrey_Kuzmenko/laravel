<?php

namespace App\Models;


class Phone extends Model
{
    //
    public $primaryKey = 'user_id';
    protected $fillable = ['user_id', 'number', 'confirm'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
