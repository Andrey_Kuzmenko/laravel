<?php

namespace App\Models;


class Salon extends Model
{
    //
    public function addresses()
    {
        return self::hasMany(Address::class);
    }
}
