<?php

namespace App\Models;


use App\Services\ImageService;
use DB;

class Master extends Model
{
    //

    public function brands()
    {
        return $this->belongsToMany(Brand::class);
    }

    public function images()
    {
        return $this->belongsToMany(Image::class, 'master_images');
    }

    public function infoImages()
    {
        $info_images = DB::table('masters')
            ->join('master_images','masters.id','=','master_images.master_id')
            ->join('images','master_images.image_id','=', 'images.id')
            ->select('image_id as id','images.name as url','sort')
            ->where('masters.id',$this->id)
            ->get();
        foreach($info_images as $image){
           $image->url = ImageService::getPath($image->url,$this->id);
    }
        return $info_images;
    }


}
