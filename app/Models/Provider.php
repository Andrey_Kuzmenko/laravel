<?php

namespace App\Models;


class Provider extends Model
{
    //
    protected $fillable = ['name', 'key', 'secret'];
}
