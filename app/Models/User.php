<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Stmt\Throw_;

class User extends Authenticatable
{


    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                            'area_id',
                            'role_id',
                            'avatar_id',
                            'background_id',
                            'name',
                            'surname',
                            'bdate',
                            'login',
                            'password',
                            'longitude',
                            'latitude',
                            'remember_token',
                            'factor',
                            'confirm_register',
                         ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function socials()
    {
        return self::hasMany(Social::class);
    }


    public function emails()
    {
        return self::hasMany(Email::class, 'user_id');
    }

    public function phones()
    {
        return self::hasMany(Phone::class);
    }

    public function avatar()
    {
        return self::belongsTo(Image::class);
    }

    public function background()
    {
        return self::belongsTo(Image::class);
    }

    public function role()
    {
        return self::belongsTo(Role::class);
    }
}
