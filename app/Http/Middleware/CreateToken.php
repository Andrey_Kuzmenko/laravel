<?php

namespace App\Http\Middleware;

use App\Models\Register;
use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CreateToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $registers_count = Register::find(Auth::id());
        $user = User::find(Auth::id());
        $confirm_register = $user->confirm_register;
        if(is_null($registers_count) && $confirm_register == 0){
            return redirect('/addNewToken');
        }
        return $next($request);
    }
}
