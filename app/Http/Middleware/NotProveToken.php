<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Closure;

class NotProveToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $users = DB::select('select confirm_register, role_id from users where id ='.Auth::id());
        var_dump($users);

        $confirm_register = $users[0]->confirm_register;
        $role_id = $users[0]->role_id;

        if ($confirm_register == 0 && $role_id == 0)
        {
            return redirect('/link');
        }

        return $next($request);
    }
}
