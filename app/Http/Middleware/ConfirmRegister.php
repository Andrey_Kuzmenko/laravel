<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Email;
use App\Models\Phone;
use App\Models\Register;
use Illuminate\Support\Facades\Validator;
use App\Services\VerificationService;

class ConfirmRegister
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (filter_var($request['login'], FILTER_VALIDATE_EMAIL)) {
            $rules['login'] = 'required|string|max:255|exists:emails,email';
        }
        else{
            $string = $request['login'];
            $request['login'] = preg_replace('~[^0-9]+~','',$string);
            $rules['login'] = 'required|string|max:255|exists:phones,number';
        }
        $rules['password'] = 'required|string|min:6';
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->all()], 400);
        }
        if (filter_var($request['login'], FILTER_VALIDATE_EMAIL)) {
            $emailInfo = Email::where('email',$request->login)->first();
            if(!is_null($emailInfo)) {

                $confirm = $emailInfo->confirm;
                if($confirm == 0){
                    $token = Register::where('login',$request->login)->first();
                    if(!is_null($token)) {
                        $token->delete();
                    }
                    VerificationService::CreateToken($request->login,0);
                    $token_new = Register::where('login',$request->login)->first();
                    return response()->json(['login' => 'email','token' => $token_new->token],409);
                }
                else{
                    return $next($request);
                }
            }
            return response()->json(['error' => 'Email do not exists'],40);
        }
        else{
            if(strlen($request['login']) !=12 ){
                return response()->json(['error' => ["password wrong format. Telefon mast have 12 digits!"]], 400);
            }
            $phoneInfo = Phone::where('number',$request->login)->first();
            if(!is_null($phoneInfo)) {
                $confirm = $phoneInfo->confirm;
                if($confirm == 0){
                    $token = Register::where('login',$request->login)->first();
                    if(!is_null($token)) {
                        $token->delete();
                    }
                    VerificationService::CreateToken($request->login,1);
                    $token_new = Register::where('login',$request->login)->first();
                    return response()->json(['login' => 'phone','token' => $token_new->token],409);
                }
                else{
                    return $next($request);
                }
            }
            return response()->json(['error' => 'Phone do not exists'],400);

        }
    }
}
