<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class LoginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $rules['password']= 'required|string|min:6|confirmed';
        $rules['role'] = 'required|integer';
        if (filter_var($request['login'], FILTER_VALIDATE_EMAIL)) {
            //$login = $request['login'];
            $rules['login'] = 'required|string|max:255|unique:emails,email';
        }
        else{
            $string = $request['login'];
            $request['login'] = preg_replace('~[^0-9]+~','',$string);
            if(strlen($request['login']) !=12 ){
                return response()->json(['error' => ["password wrong format. Telefon mast have 12 digits!"]], 400);
            }
            $rules['login'] = 'required|string|max:255|unique:phones,number';
        }
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->all()], 400);
        }
        return $next($request);
    }
}
