<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Auth;
use App\Models\Master;
use Illuminate\Support\Facades\Input;

class MasterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //разрешения
        // confirm = подтверждение сессии и переданного ключа
        $allow = [
            'Master'=>[
                'action'=>['update', 'store', 'delete'],
                'confirm'=>true
                ],
            'Admin'=>[
                'action'=>['delete'],
                'confirm'=>false
            ]
        ];

        $id = ($request->route()->parameters())['id'];
        if(!Master::find($id)){
            return response()->json(['error'=>'NOT_USER'], 403);
        }

        $action = $request->route()->getActionMethod();

        //Найти ID пользователя по сессии
        $session_user_id = Auth::id();

        if(!$session_user_id){
            return response()->json('', 403);
        }

        $role = User::find($session_user_id)->role;
        if(!$role || !array_key_exists($role->name,$allow)){
            return response()->json('', 403);
        }

        if(!in_array($action, $allow[$role->name]['action'])){
            return response()->json('', 403);
        }

        if($allow[$role->name]['confirm']){
            if($session_user_id!==$id){
                return response()->json('', 403);
            }
        }

        return $next($request);
    }
}
