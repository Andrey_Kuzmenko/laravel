<?php

namespace App\Http\Controllers;

use App\Models\Phone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Services\VerificationService;

class PhoneController extends Controller
{
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id'=> 'required|integer',
            'number' => 'required|string|max:255|unique:phones',
            'primary' => 'integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        if($request->primary == 1){
            $checkPrimary = Phone::where('user_id',$request->user_id)->where('primary',$request->primary)->count();
            if($checkPrimary > 0){
                return response()->json(['error' => ['Phone have already primary']], 400);
            }
        }
        $addPhone = new Phone();
        $addPhone->user_id = $request->user_id;
        $addPhone->number = $request->number;
        $addPhone->primary = $request->primary;
        if($addPhone->save()){
            $addtoken = VerificationService::addNewToken($request->number,1);
            return response()->json(['success add' => ['Phone was add',]], 201);
        }
        return response()->json(['error' => ['Phone was not add']], 400);
    }

    public function update($old_phone,Request $request){
        $rules = $request->all();
        $rules['old_phone'] = $old_phone;
        $validator = Validator::make($rules, [
            'user_id'=> 'required|integer',
            'old_phone' => 'required|string|max:255|exists:phones,number',
            'number' => 'required|string|max:255',
            'primary' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        if($request->primary ==1){
            $checkPrimary = Phone::where('user_id',$request->user_id)->where('primary',$request->primary)->count();
            if($checkPrimary > 0){
                return response()->json(['error' => ['Phone have already primary']], 400);
            }
        }
        $updatePhone = Phone::where('number',$rules['old_phone'])->update(['user_id' => $request->user_id,'number' => $request->number,'primary' => $request->primary]);
        if($updatePhone){
            return response()->json(['success' => ['Phone updated!']], 200);
        }
        return response()->json(['error' => ['Phone not updated']], 400);
    }

    public function destroy($old_phone){
        $rules['number'] = $old_phone;
        $validator = Validator::make($rules, [
            'number' => 'required|string|max:255|exists:phones',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        $deleteEmail = Phone::where('number',$old_phone)->delete();
        if($deleteEmail){
            return response()->json(['success' => ['Phone was deleted!']], 200);
        }
        else{
            return response()->json(['error' => ['Phone was not deleted!']], 400);
        }
    }
}