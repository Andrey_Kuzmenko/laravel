<?php

namespace App\Http\Controllers;

use App\Provider;
use Illuminate\Http\Request;
use Socialite;
use App\Services\UserService;
use Auth;

use App\UserSocialAccount;

class SocialController extends Controller
{
    //

    public function login($provider_soc)
    {

        $provider = Provider::where('name',$provider_soc)->first();

        $config = new \SocialiteProviders\Manager\Config($provider->key, $provider->secret, $provider->redirect_url);


        $social = Socialite::with($provider->name);


        return $social->redirect();

    }


    public function callback($provider_name)
    {
        try{
            $user = UserService::createOrGetSocial($provider_name);
        }catch(ClientException $e){
            abort(400);
        }catch(Exception $e){
            abort(400);
        }

        
        Auth::login($user);


        return redirect()->route('main');

    }

}
