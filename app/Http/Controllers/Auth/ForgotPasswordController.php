<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\PasswordReset;
use App\Services\ResetService;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    // use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showLinkRequestForm()
    {
        return view('auth.passwords.email');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $token = "";
        if (filter_var($request['login'], FILTER_VALIDATE_EMAIL)) {
            //$login = $request['login'];
            $token = str_random(60);
            $rules = array([
                'login' => 'required|string|max:255|exists:emails,email',
            ]);
        }
        else{
            $string = $request['login'];
            $request['login'] = preg_replace('~[^0-9]+~','',$string);
            $rules = array([
                'login' => 'required|string|max:255|exists:phones,number',
            ]);
            for($i = 0; $i < 6; $i++)
            {
                $token.=rand(0,9);
            }
        }
        $validator = Validator::make($request->all(),$rules);
        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->all()], 400);
        }
        return ResetService::ResetTokenAdd($request,$token);

    }
    public function checkToken($token)
    {
        return ResetService::checkToken($token);
    }
    public function checkPhoneToken(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'required|string|max:255|exists:password_resets',
        ]);
        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->all()], 400);
        }

        return ResetService::checkToken($request->token);
    }
    public function updatePassword(Request $request){
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
        ]);
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $updatePassword = User::find(Auth::id);
        $updatePassword->password = bcrypt($request->password);
        if($updatePassword->save()){
            return response()->json(['success' => 'Password update'], 200);
        }
        return response()->json(['error' => 'Password not update'], 400);
    }
}
