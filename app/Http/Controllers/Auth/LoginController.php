<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Email;
use App\Models\Phone;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Cache\RateLimiter;
use App\Services\VerificationService;
use App\Models\Register;
use Illuminate\Support\Facades\Hash;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $rules['password'] = 'required|string';
        if (filter_var($request->login, FILTER_VALIDATE_EMAIL)) {
            $rules[$this->username()] = 'required|string|exists:emails,email';
            $type = 0;
        }
        else{
            $string = $request->login;
            $request["login"] = preg_replace('~[^0-9]+~','',$string);
            $rules[$this->username()] = 'required|string|exists:phones,number';
            $type = 1;
        }
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->all()], 400);
        }
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if($type == 0){
            $emailInfo = Email::where('email',$request->login)->first();
            $id = $emailInfo->user_id;
            $confirm = $emailInfo->confirm;
        }
        else{
            $phoneInfo = Phone::where('number',$request->login)->first();
            $id = $phoneInfo->user_id;
            $confirm = $phoneInfo->confirm;
        }

        $userInfo = User::find($id);
        if (Hash::check($request->password, $userInfo->password)) {
            if ($userInfo->factor == 0) {

                /*if ($this->attemptLogin($request)) {
                    dd($request->all());
                    return $this->sendLoginResponse($request);
                }*/

                Auth::loginUsingId($id);
                return $this->sendLoginResponse($request);
            }
            else{
                return self::showTwoFactor($request->login);
            }
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        return $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        /* $this->validate($request, [
             $this->username() => 'required|string',
             'password' => 'required|string',
         ]);*/
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->has('remember')
        );
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $this->clearLoginAttempts($request);
        return $this->authenticated($request, $this->guard()->user());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        return response()->json(['success ' => ['Login complete']],200);
    }
    protected function showTwoFactor($login){
        if (filter_var($login, FILTER_VALIDATE_EMAIL)) {
            //return VerificationService::addNewToken(Auth::id(), 0);
            $emailInfo = Email::where('email',$login)->first();
            $id = $emailInfo->user_id;
            $phonePrimary = Phone::where('user_id',$id)->where('primary',1)->first();
            VerificationService::CreateToken($phonePrimary->number, 1);
            $tokenInfo = Register::where('login',$phonePrimary->number)->first();
            return response()->json(['factor' => true, 'token' => $tokenInfo->token], 200);
        }
        else {
            VerificationService::CreateToken($login, 1);
            $tokenInfo = Register::where('login',$login)->first();
            return response()->json(['factor' => true, 'token' => $tokenInfo->token], 200);
        }
    }
    protected function TwoFactor(Request $request)
    {
        $fileArray = array('token' => $request->token);
        $rules = array(
            'token' => 'required'
        );
        $validator = Validator::make($fileArray, $rules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        $phone = Register::where('token',$request->token)->count();
        if($phone > 0){
            $reg = Register::where('token', '=', $request->token)->first();
            $phoneInfo = Phone::where('number',$reg->login)->first();
            Auth::loginUsingId($phoneInfo->user_id);
            $reg->delete();
            return response()->json(['success' => ['User Confirmed']],200);
        }
        $deleteToken = Register::where('login',$request->login)->first();
        $deleteToken->delete();
        return response()->json(['error' => ['User do not Confirmed']],400);
    }
    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];
        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        /* return redirect()->back()
             ->withInput($request->only($this->username(), 'remember'))
             ->withErrors($errors);*/
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'login';
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();
        return response()->json(['success' => ['Logout Complete']],200);
        //return redirect('/',302);
        //return redirect('/');
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
    /**
     * Determine if the user has too many failed login attempts.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request), 5, 1
        );
    }

    /**
     * Increment the login attempts for the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function incrementLoginAttempts(Request $request)
    {
        $this->limiter()->hit($this->throttleKey($request));
        return response()->json(['error' =>['Login not found'] ], 400);
    }

    /**
     * Redirect the user after determining they are locked out.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        $message = Lang::get('auth.throttle', ['seconds' => $seconds]);

        $errors = [$this->username() => $message];

        if ($request->expectsJson()) {
            return response()->json($errors, 423);
        }

       /* return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);*/
    }

    /**
     * Clear the login locks for the given user credentials.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function clearLoginAttempts(Request $request)
    {
        $this->limiter()->clear($this->throttleKey($request));
    }

    /**
     * Fire an event when a lockout occurs.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function fireLockoutEvent(Request $request)
    {
        event(new Lockout($request));
    }

    /**
     * Get the throttle key for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function throttleKey(Request $request)
    {
        return Str::lower($request->input($this->username())).'|'.$request->ip();
    }

    /**
     * Get the rate limiter instance.
     *
     * @return \Illuminate\Cache\RateLimiter
     */
    protected function limiter()
    {
        return app(RateLimiter::class);
    }
}
