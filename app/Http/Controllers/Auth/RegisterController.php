<?php

namespace App\Http\Controllers\Auth;

use App\Models\Register;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Models\Master;
use App\Models\Email;
use App\Models\Phone;
use App\Services\VerificationService;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

   // use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
        public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        event(new Registered($user = $this->create($request->all())));
       // $this->guard()->login($user);
        return $this->registered($request, $user);
    }
     /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
      $user =  User::create([
          //  'login' => $data['login'],
            'password' => bcrypt($data['password']),
            'role_id' => $data['role'],
        ]);
       $id = $user->id;
       if(intval($data['role']) == 3){
            $addMaster = new Master();
            $addMaster->id = $id;
            $addMaster->save();
       }

        if (filter_var($data['login'], FILTER_VALIDATE_EMAIL)) {
            $addEmail = new Email;
            $addEmail->user_id = $id;
            $addEmail->email = $data['login'];
            $addEmail->primary = 1;
            $addEmail->save();
            VerificationService::addNewToken($data['login'],0);
        }
        else{
            $addPhone = new Phone();
            $addPhone->user_id = $id;
            $addPhone->number = $data['login'];
            $addPhone->primary = 1;
            $addPhone->save();
            VerificationService::addNewToken($data['login'],1);
        }
        return $user;
    }
    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        $reg = Register::where('login',$request->login)->first();
        return response()->json(['success ' => 'Registrate compleate','token'=>$reg->token],201);
    }
}
