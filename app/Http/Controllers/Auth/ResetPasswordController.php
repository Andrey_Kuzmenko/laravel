<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Email;
use App\Models\PasswordReset;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

   // use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }
    public function showResetForm()
    {
        return view('resetPassword');
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6|confirmed',
            'token' =>'required|string',
        ]);
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors()->all()], 400);
        }
        $tokenInfo = PasswordReset::where('token',$request->token)->first();
        $emailInfo = Email::where('email',$tokenInfo->login)->first();
        $updatePassword = User::find($emailInfo->user_id);
        $updatePassword->password = bcrypt($request->password);
        $afterUpd = PasswordReset::where('login',$tokenInfo->login)->delete();
        if($updatePassword->save()){
            Auth::loginUsingId($emailInfo->user_id);
            return response()->json(['success' => 'Password update'], 200);
        }
        return response()->json(['error' => 'Password not update'], 400);
    }
}
