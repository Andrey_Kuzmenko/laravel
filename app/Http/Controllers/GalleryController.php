<?php

namespace App\Http\Controllers;

use App\Models\Master;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Validator;
use App\Models\Image;
use App\Models\MasterImage;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($master_id)
    {
        //
        $master = Master::find($master_id);
        $info_images = $master->infoImages();
        return response()->json($info_images, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        $master_id = (Master::find($id))->id;
        $count_image = MasterImage::where('master_id', $master_id)->count();
        if ($count_image > MasterImage::MAX_COUNT_IMAGE) {
            return response()->json(['error' => 'EXESEEDED_LIMIT_MAX_COUNT'], 400);
        }

        $uploadImage = $request->all();
        $validator = Validator::make($uploadImage, ['image' => 'required|mimes:jpeg,jpg,png,gif|max:' . Image::MAX_SIZE]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }

        $imageNameFile = (new ImageService($uploadImage['image']))->save($master_id);

        $image = Image::create(
            [
                'name' => $imageNameFile,
            ]
        );
        $m_image = MasterImage::create(
            [
                'master_id' => $master_id,
                'image_id' => $image->id,
                'sort' => MasterImage::where('master_id', $master_id)->max('sort') + 1
            ]
        );
        $data = [
            'id' => $image->id,
            'url' => ImageService::getPath($image->name, $master_id),
            'sort' => $m_image->sort,
        ];
        return response()->json($data, 200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $req_images_info = $request->all();
        foreach($req_images_info as $image_info){
              MasterImage::where([
                        'master_id'=>$id,
                        'image_id'=>$image_info['id']])
                    ->update(['sort'=>$image_info['sort']]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($master_id, $id)
    {
        //
        $img = Image::find($id);
        if (!$img) {
            return response()->json(['error'=>'NOT_FIND_IMAGE'], 404);
        }
        ImageService::delete($img->name,$master_id);
        $img->delete();

        return response()->json('Ok', 200);
    }

}
