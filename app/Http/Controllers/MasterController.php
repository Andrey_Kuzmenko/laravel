<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\vFindMaster;
use Illuminate\Support\Facades\Validator;
use App\Models\Service;
use App\Models\vMasterService;


class MasterController extends Controller
{
    public function __construct()
    {
       // $this->middleware('auth');
    }

    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'max:255',
            'city' => 'max:255'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        if (is_null($request->city)) {
            $findResultName = vFindMaster::where('name', 'like', $request->name . "%")->get();
            $findResultSurn = vFindMaster::where('surname', 'like', $request->name . '%')->get();
            $findResultServ = vMasterService::where('name_service','like', $request->name . '%')->get();
            return response()->json(['success' => $findResultName, $findResultSurn,$findResultServ], 200);
        }
        if(!is_null($request->city) && !is_null($request->name)){
            $findResultName = vFindMaster::where('name', 'like', $request->name . "%")->where('city', 'like', $request->city . "%")->get();
            $findResultSurn = vFindMaster::where('surname', 'like', $request->name . '%')->where('city', 'like', $request->city . "%")->get();
            $findResultServ = vMasterService::where('name_service','like', $request->name . '%')->where('city', 'like', $request->city . "%")->get();
            return response()->json(['success' => $findResultName, $findResultSurn,$findResultServ], 200);
        }
        $findResultCity = vFindMaster::where('city', 'like', $request->city.'%')->get();
        return response()->json(['success' => $findResultCity], 200);
    }
}