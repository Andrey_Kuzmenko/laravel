<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Service;
use App\Models\vServiceImg;
use App\Models\MasterBrand;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;
use App\Services\MasterService;
use App\Models\Image;
use App\Services\ImageService;
class ServiceController extends Controller
{
    public function index(){
        $serviceAll = vServiceImg::all();
        return response()->json($serviceAll, 200);
    }
    public function show($id){
        $serviceUsers = vServiceImg::where('master_id',$id)->skip(0)->take(15)->get();
        foreach ($serviceUsers as $serviceUser){
            if($serviceUser->img_name !=NULL) {
                $serviceUser->img_name = ImageService::getPath($serviceUser->img_name,$serviceUser->master_id);
            }
        }
        $count = Service::where('master_id',$id)->count();
        return response()->json(['skills' => $serviceUsers,'count' =>$count], 200);
    }
    public function showOne($id){
        $fileArray = array('id' => $id);
        $rules = array(
            'id' => 'required|exists:services'
        );
        $validator = Validator::make($fileArray, $rules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        $service = vServiceImg::find($id);
        if($service->img_name !=NULL) {
                $service->img_name = ImageService::getPath($service->img_name,$service->master_id);
            }
        return response()->json(['service' => $service], 200);
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id' =>'required|integer',
            'name' => 'required|string|max:255|unique:services,name',
            'category_id' => 'required|integer|exists:categories,id',
            'price_min' => 'regex:/^(\d*\.)(\d{2})$/',
            'price_max' => 'regex:/^(\d*\.)(\d{2})$/',
            'time' => 'integer|min:15',
            'note' => 'string',
            'image' => 'mimes:jpeg,jpg,png,gif|max:'.Image::MAX_SIZE,
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        if(!is_null($request->image)) {
            $img = new ImageService($request->image);
            $name = $img->save($request->user_id);
            $img = new Image();
            $img->name = $name;
            $img->save();
            $imgId = $img->id;
        }
        else{
            $imgId = NULL;
        }
        $addMasterService = new Service();
        $addMasterService->master_id = $request->user_id;
        $addMasterService->image_id = $imgId;
        $addMasterService->category_id = $request->category_id;
        $addMasterService->name = $request->name;
        $addMasterService->price_min = $request->price_min;
        $addMasterService->price_max = $request->price_max;
        $addMasterService->time = $request->time;
        $addMasterService->note = $request->note;
        $addMasterService->timestamps = false;
       if($addMasterService->save()) {
           return response()->json(['success' => ['Service was add!']], 201);
       }
       else{
           return response()->json(['success' => ['Service was not add!']], 400);
       }
    }
    public function updatePost($id,Request $request){
        $fileArray = array('id' => $id);
        $rules = array(
            'id' => 'required|exists:services'
        );
        $validator = Validator::make($fileArray, $rules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        $validator = Validator::make($request->all(), [
            'user_id' =>'required|integer',
            'name' => 'required|string|max:255',
            'category_id' => 'required|integer|exists:categories,id',
            'price_min' => 'regex:/^(\d*\.)(\d{2})$/',
            'price_max' => 'regex:/^(\d*\.)(\d{2})$/',
            'time' => 'integer|min:15',
            'note' => 'string',
            'image' => 'mimes:jpeg,jpg,png,gif|max:'.Image::MAX_SIZE,
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        $imgId = NULL;
        if(!is_null($request->image)) {
            $img = new ImageService($request->image);
            $name = $img->save($request->user_id);
            $img = new Image();
            $img->name = $name;
            $img->save();
            $imgId = $img->id;
        }
        $updateService = Service::Find($id);
        $updateService->master_id = $request->user_id;
        if(!is_null($imgId)){
            $updateService->image_id = $imgId;
        }
        $updateService->name = $request->name;
        $updateService->note = $request->note;
        $updateService->category_id = $request->category_id;
        $updateService->price_min =$request->price_min;
        $updateService->price_max = $request->price_max;
        $updateService->time = $request->time;
        if($updateService->save()) {
            return response()->json(['success' => ['Service was update!']], 200);
        }
        else{
            return response()->json(['success' => ['Service was not update!']], 400);
        }
    }
    public function destroy($id){
        $fileArray = array('id' => $id);
        $rules = array(
            'id' => 'required|exists:services'
        );
        $validator = Validator::make($fileArray, $rules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        $updateService = Service::Find($id);
        if($updateService->delete()){
            return response()->json(['success' => ['Service was deleted!']], 200);
        }
        else{
            return response()->json(['error' => ['Service was not deleted!']], 400);
        }
    }

}