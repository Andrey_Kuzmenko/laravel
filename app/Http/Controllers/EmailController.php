<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Email;
use Illuminate\Support\Facades\Validator;
use App\Services\VerificationService;

class EmailController extends Controller
{
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'user_id'=> 'required|integer',
            'email' => 'required|email|max:255|unique:emails',
            'primary' => 'integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        if($request->primary ==1){
            $checkPrimary = Email::where('user_id',$request->user_id)->where('primary',$request->primary)->count();
            if($checkPrimary > 0){
                return response()->json(['error' => ['Email have already primary']], 400);
            }
        }
        $addEmail = new Email();
        $addEmail->user_id = $request->user_id;
        $addEmail->email = $request->email;
        $addEmail->primary = $request->primary;
        if($addEmail->save()){
            $addtoken = VerificationService::addNewToken($request->email,0);
            return response()->json(['success' => ['Email was add']], 201);
        }
        return response()->json(['error' => ['Email not  add']], 400);
    }
    public function update($old_email,Request $request){
        $rules = $request->all();
        $rules['old_email'] = $old_email;
       // dd($rules);
        $validator = Validator::make($rules, [
            'user_id'=> 'required|integer',
            'old_email' => 'required|email|max:255|exists:emails,email',
            'email' => 'required|email|max:255',
            'primary' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        $updateEmail =  Email::where('email',$rules['old_email'])->update(['user_id' => $request->user_id,'email' => $request->email,'primary' => $request->primary]);
        if($updateEmail){
            $addtoken = VerificationService::addNewToken($request->email,0);
            return response()->json(['success' => ['Email updated!']], 200);
        }
        return response()->json(['error' => ['Email not updated']], 400);
    }
    public function destroy($old_email){
        $rules['email'] = $old_email;
        $validator = Validator::make($rules, [
            'email' => 'required|string|max:255|exists:emails',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        $deleteEmail = Email::where('email',$old_email)->delete();
        if($deleteEmail){
            return response()->json(['success' => ['Email was deleted!']], 200);
        }
        else{
            return response()->json(['error' => ['Email was not deleted!']], 400);
        }
    }
}