<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use Illuminate\Http\Request;
use App\Models\vMasterBrand;
use Illuminate\Support\Facades\Validator;
use App\Services\BrandService;

class BrandController extends Controller
{
    public function index(){
        $brands = Brand::all();
        return response()->json($brands, 200);
    }
    public function show($id){
        $brands = vMasterBrand::where('master_id',$id)->get();
        return response()->json($brands, 200);
    }
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:brands,name',
            'id' => 'integer',
        ]);
        //  var_dump($validator->errors());
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        return BrandService::addBrand($request);
    }
    public function destroy($id){
        $deleteBrand = Brand::find($id);
        if($deleteBrand->delete()){
            return response()->json(['success' => ['Brand delete success']], 200);
        }
        return response()->json(['error' => ['Brand not delete']], 400);
    }
    public function update($id,Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        $updateBrand = Brand::find($id);
        $updateBrand->name = $request->name;
        if($updateBrand->save()){
            return response()->json(['success' => ['Brand update success']], 200);
        }
        return response()->json(['error' => ['Brand not update ']], 400);
    }
}