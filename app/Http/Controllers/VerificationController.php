<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Mail\MailSender;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon;
use Illuminate\Support\Facades\Mail;
use App\Services\VerificationService;
use Illuminate\Support\Facades\Validator;
use Nexmo\Laravel\Facade\Nexmo;

class VerificationController extends Controller
{

    public function addNewToken()
    {
        $user = User::find(Auth::id());
        $login = $user->login;
        if ($login != NULL) {
            if (filter_var($login, FILTER_VALIDATE_EMAIL)) {
                return VerificationService::addNewToken(Auth::id(), 0);
            } else {
                return VerificationService::addNewToken(Auth::id(), 1);
            }
        } else {
            return response()->json(['error' => 'Login is NULL'], 400);
        }
    }

    /**
     * @param $token
     */
    public function verification($token)
    {
        return VerificationService::verification($token);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verificationPhone(Request $request)
    {
        return VerificationService::verificationPhone($request);
    }
    /**
     * @param $role
     * @return \Illuminate\Http\JsonResponse
     */
    /*public function chooseRole($role)
    {
        $fileArray = array('role' => $role);
        $rules = array(
            'role' => 'required|integer'
        );
        $validator = Validator::make($fileArray, $rules);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        $updateUser = User::find(Auth::id());
        $updateUser->role_id = $role;
        $updateUser->save();
        return response()->json(['success' => 'User roll update'],201);
    }*/
}
