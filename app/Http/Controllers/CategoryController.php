<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\vServiceImg;
use App\Models\vMasterCategory;
use App\Services\ImageService;

class CategoryController extends Controller
{
    public function index(){
        $allCategories =  Category::all();
        return response()->json(['success' => $allCategories], 200);
    }

    public function show($id){
        $mastersCategories = vMasterCategory::where('master_id',$id)->get();
        return response()->json(['success' => $mastersCategories], 200);
    }

    public function showCurrent($mast_id,$cat_id){
        $mastersCategories = vServiceImg::where('master_id',$mast_id)->where('category_id',$cat_id)->get();
        foreach ($mastersCategories as $mastersCategory){
            if($mastersCategory->img_name !=NULL) {
                $mastersCategory->img_name = ImageService::getPath($mastersCategory->img_name,$mastersCategory->master_id);
            }
        }
        return response()->json(['success' => $mastersCategories], 200);
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255|unique:categories,name',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        $addMasterCategory = new Category();
        $addMasterCategory->name = $request->name;
        $addMasterCategory->timestamps = false;
        if($addMasterCategory->save()) {
            return response()->json(['success' => ['Category was add!']], 201);
        }
        else{
            return response()->json(['success' => ['Category was not add!']], 400);
        }
    }

    public function update($id,Request $request){
        $rules = $request->all();
        $rules['id'] = $id;
        $validator = Validator::make($rules, [
            'id' => 'required|string|exists:categories',
            'name' => 'required|string',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        $updateMasterCategory = Category::find($id);
        $updateMasterCategory->name = $request->name;
        $updateMasterCategory->timestamps = false;
        if($updateMasterCategory->save()) {
            return response()->json(['success' => ['Category was updated!']], 200);
        }
        else{
            return response()->json(['success' => ['Category was not updated!']], 400);
        }

    }

    public function destroy($id){
        $rules['id'] = $id;
        $validator = Validator::make($rules, [
            'id' => 'required|integer|exists:categories'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        $deleteMasterCategory = Category::find($id);
        if($deleteMasterCategory->delete()){
            return response()->json(['success' => ['Category was deleted!']], 200);
        }
        else{
            return response()->json(['success' => ['Category was not deleted!']], 400);
        }
    }
}
