<?php

namespace App\Providers;

use App\SocialProfile\Contracts\Profile;
use Illuminate\Support\ServiceProvider;

class SocialProfileServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
        
        $this->app->singleton(Profile::class, function ($app) {
            return new \App\SocialProfile\Models\VkontakteSocialProfile($app);
        });
    }
}
