<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app['validator']->extend('phoneEmail', function ($attribute, $value, $parameters)
        {
            $re = '/\+[0-9]{1,2}[0-9]{10}/';
            if (filter_var($value, FILTER_VALIDATE_EMAIL)|| preg_match_all($re, $value, $matches, PREG_SET_ORDER, 0)){
                return true;
            }
            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
