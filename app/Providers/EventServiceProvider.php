<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use DB;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
        ],

        \SocialiteProviders\Manager\SocialiteWasCalled::class => [

            // Порписываем здесь обработку события провайдерами от SocialiteProviders
            'App\Social\Vkontakte\VKontakteExtendSocialite@handle',
            'SocialiteProviders\Google\GoogleExtendSocialite@handle',

            ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {

        /*DB::listen(function($sql) {
            dump($sql);
        });*/
        parent::boot();

        //
    }
}
