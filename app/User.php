<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use PhpParser\Node\Stmt\Throw_;

class User extends Authenticatable
{
    const USER_STATUS_ACTIVE = 1;


    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'area_id','role_id','avatar_id','background_id','name','surname','bdate','email', 'password', 'longitude','latitude', 'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function provider()
    {
        return $this->belongsTo('/App/Provider');
    }

    static public function createOrGet($provider_name)
    {
        $provider = Provider::where('name',$provider_name)->first();

        $user_social = new UserSocialAccount($provider_name);


        $user = User::where('email',$user_social->email)->first();
        
        dd($user);
        if(!$user && $user->social_user_id===null){

            $fields = $user_social->getFields();

            $fields['provider_id'] = $provider->id;
            $fields['role_id'] = User::USER_STATUS_ACTIVE;

            $user = User::create($fields);

            return $user;
        }
           // dd($user_social->social_user_id);
        $a = ($user->social_user_id==$user_social->social_user_id && $provider->id == $user->provider_id);

        if($a){
            return $user;
        }

        dump($user);
        dd($user_social);
        return false;
    }
}
