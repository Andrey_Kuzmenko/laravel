<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery_Master extends Model
{
    protected $table = 'master_galleries';
}
