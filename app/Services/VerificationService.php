<?php
namespace App\Services;
use App\Models\Email;
use App\Models\Phone;
use App\Models\Register;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailSender;
use Nexmo\Laravel\Facade\Nexmo;
use Illuminate\Support\Facades\Validator;

class VerificationService
{
    const LOGIN_EMAIL = 0;
    const LOGIN_PHONE = 1;

    /**
     * @param $id
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    public static function addNewToken($login,$type){
        $registers_count = Register::where('login',$login)->first();
        if($type == self::LOGIN_EMAIL){
            $loginInfo = Email::where('email',$login)->first();
        }
        else{
            $loginInfo = Phone::where('number',$login)->first();
        }
       // $user = User::find($id);
        $confirm_register = $loginInfo->confirm_register;
        if(is_null($registers_count) && $confirm_register == 0) {
            return self::CreateToken($login,$type);
        }
        elseif(!is_null($registers_count)){
            $errors['token exists'] ="token exists";
        }
        else{
            $errors['token exists'] ="user verified";
        }
        return response()->json(['error' => $errors], 400);

    }

    /**
     * @param $id
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    public static function CreateToken($login,$type){
       // $user = User::find($id);
        if($type == VerificationService::LOGIN_EMAIL) {
            $newToken = str_random(60);
        }
        else{
            if($type == VerificationService::LOGIN_PHONE){
                $newToken = "";
                for($i = 0; $i < 6; $i++)
                {
                    $newToken.=rand(0,9);
                }
            }
        }
        $register = new Register;
        $register->login = $login;
        $register->token = $newToken;
        $register->save();
        if($type == VerificationService::LOGIN_EMAIL) {
            $email = $login;
            $registers = Register::where('login',$login)->first();
            $token = $registers->token;
            $link = url('verification') . '/' . $token;
            // echo $link;
            $to      = $email;
            $subject = 'Подтверждение аккаунта';
            $message = 'Ваша ссылка на подтверждение аккаунта:'.$link;
            mail($to, $subject, $message);
            // Mail::to($email)->send(new MailSender($link));
            // echo "Токен создан";
            return response()->json(['success' => 'Token create and send to email'],201);
            // var_dump($link);
        }
        if($type == VerificationService::LOGIN_PHONE) {
            $phone = $login;
            $registers = Register::where('login',$login)->first();
            $token = $registers->token;
            /* Nexmo::message()->send([
            'to' => '380957354562',
            'from' => '380957354562',
            'text' => 'Enter the code to form to confirm registration.'.$token
                ]);*/
            return response()->json(['success' => 'Token create and send to phone'],201);
        }

    }

    /**
     * @param $token
     * @return \Illuminate\Http\JsonResponse
     */
    public static function verification($token){
       $verification = Register::where('token','=',$token)->count();
       // var_dump($verification);
        if($verification >0){
            $registerInfo = Register::where('token',$token)->first();
            $login = $registerInfo->login;
           //dd($login);
            var_dump(strlen($token));
            if(strlen($token) > 6){
               $findLogin = Email::where('email',$login)->first();
               //var_dump($findLogin);
            }
            else{
                $findLogin = Phone::where('number',$login)->first();
            }
            $findLogin->confirm = 1;
            if($findLogin->save())
            {
                $reg = Register::where('token', '=', $token)->first();
                $reg->delete();
            }
            //return redirect()->to('/',302);
            return response()->json(['success' => 'user verificate'],200);
        }
        return response()->json(['error' => 'user do not verificate'],400);
    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public static function verificationPhone($request){
        $validator = Validator::make($request->all(), [
            'token' => 'required|string|min:6|max:6',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->getMessages()], 400);
        }
        else {
            return self::verification($request->token);
        }
    }

}