<?php
/**
 * Created by PhpStorm.
 * User: avrora
 * Date: 5/10/17
 * Time: 1:26 PM
 */
namespace App\Services;
use App\Models\Image;
use App\Models\User;
use App\Models\Master;
use App\Models\MasterGallery;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class MasterService
{
    const IMAGE_AVATAR = 0;
    const IMAGE_BACKGROUND = 1;
    static public $disk = "img";
    static public function uploadImage($image,$imageName,$type)
    {
        Storage::disk(self::$disk)->put(Auth::id(), $image);
        $imageAdd = new Image();
        $imageAdd->name = $imageName;
        $imageAdd->save();
        $idImage = Image::where('name','=',$imageName)->first();
        $masterUpdate = User::find(Auth::id());
        var_dump($masterUpdate);
        if(intval($type) == self::IMAGE_AVATAR){
            $masterUpdate->avatar_id = $idImage->id;
        }
        elseif(intval($type) == self::IMAGE_BACKGROUND){
            $masterUpdate->background_id = $idImage->id;
        }
        $masterUpdate->save();
    }
    static public function addGallery($image){
        $userInfo = User::where('id', '=', Auth::id())->first();
        var_dump($userInfo);
        $photoCount = MasterGallery::where('master_id','=',Auth::id())->count();
        $userRoll = $userInfo->role_id;
        if($userRoll = 4 || $photoCount < 16){
            Storage::disk(self::$disk)->put(Auth::id(), $image);
            $imageAdd = new Image;
            $imageAdd->name = $image->hashName();
            $imageAdd->save();
            $idImage = Image::where('name','=',$image->hashName())->first();
            $addToGallery = new MasterGallery();
            $addToGallery->image_id = $idImage->id;
            $addToGallery->master_id = Auth::id();
            $addToGallery->save();
        }
        else{
            if($userRoll != 4){
                $errorArray['role_id'] = "Пользователь не Пермиум!";
            }
            else{
                $errorArray['count_img'] = "Пользователь уже добавил лимит фотографий";
            }
            return response()->json(['error' => $errorArray], 400);
        }
    }

}