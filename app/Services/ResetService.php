<?php
namespace App\Services;
use App\Models\Image;
use App\Models\User;
use App\Models\Master;
use App\Models\MasterGallery;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\PasswordReset;

class ResetService
{
    public static function ResetTokenAdd($request,$token)
    {
        $add = new PasswordReset();
        $add->login =$request->login;
        $add->token =$token;
        if($add->save()){
            if (filter_var($request['login'], FILTER_VALIDATE_EMAIL)) {
                $link = url('/#!/reset/password') . '/' . $token;
                echo $link;
                $to = $request->login;
                $subject = 'Скинуть пароль для юзера' . $request->login;
                $message = 'Ваша ссылка на замену пароля'.$link;
                mail($to, $subject, $message);
                return response()->json(['success' => 'resetToken send to @mail!'], 200);
            }
            else{
                /* Nexmo::message()->send([
            'to' => '380957354562',
            'from' => '380957354562',
            'text' => 'Enter the code to form to confirm registration.'.$token
                ]);*/
                return response()->json(['success' => 'resetToken send to phone! Token = '.$token], 200);
            }
            //return self::checkToken($token,$request->login);
        }
        else{
            return response()->json(['error' => 'resetToken not add!'], 400);
        }

    }
    public static function checkToken($token){
        $count = PasswordReset::where('token',$token)->count();
        if($count > 0){
            //return response()->json(['success' => 'You are logged in'], 200);
            return redirect('/#!/resetPassword',302);
        }
    }

}