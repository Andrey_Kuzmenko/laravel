<?php
/**
 * Created by PhpStorm.
 * User: avrora
 * Date: 5/10/17
 * Time: 1:26 PM
 */

namespace App\Services;
use App\Models\Brand;
use App\Models\MasterBrand;
use Illuminate\Support\Facades\Auth;

class BrandService
{
    static public function addBrand($request){
        $error = array();
        $success = array();
        $addBrand = new Brand();
        $addBrand->name = $request->name;
        if($addBrand->save()) {
            $success[] = "Added new brand success!";
        }
        else{
            $error[] = "Added new brand error!";
        }
        if(!is_null($request->id)) {
            $infoNew = Brand::where('name', $request->name)->first();
            $addMasterBrand = new MasterBrand();
            $addMasterBrand->master_id = Auth::id();
            $addMasterBrand->brand_id = $infoNew->id;
            if ($addMasterBrand->save()) {
                $success[] = "Added a link between the user and the brand!";
            } else {
                $error[] = "Added a link between the user and the brand ,error!";
            }
        }
        if(count($error) == 0){
            return response()->json(['success' => $success], 201);
        }
        return response()->json(['error' => $error], 400);
    }

}