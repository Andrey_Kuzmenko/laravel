<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 09.05.2017
 * Time: 10:24
 */

namespace App\Services;

use App\Models\User;
use App\SocialProfile\SocialProfile;
use App\Models\Provider;
use App\Models\Social;
use App\Models\Email;
use App\Models\Phone;
use App\Models\Image;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\JsonEncodingException;

class UserService
{
    const STATUS_CONFIRMED = 1;
    const STATUS_NOT_CONFIRMED = 0;

    static public function createOrGetSocial($provider_name)
    {
        $provider = Provider::where('name', $provider_name)->first();
        $social_profile = SocialProfile::make($provider_name);

        //Найти пользователя по социальному ключу

        $social = Social::where('social_id', $social_profile->id)->first();

        if ($social) {
            return $social->user;
        }

        //Найти пользователя по email
        $email = Email::where('email', $social_profile->email)->first();

        if($email){
            //если пользователь не подтвердил email, удалить данные о email и создать нового
            if($email->confirm===self::STATUS_NOT_CONFIRMED){
                Email::where('email',$email->email)->delete();
                return self::save($provider, $social_profile);

            }
            $user= $email->user;
            self::saveProvider($user->id, $provider->id, $social_profile->id);
            return $user;
        }


        //Найти пользователя по  телефону
    /*    $phone = Phone::where('number', $social_profile->phone)->first();

        if($phone){
            //если пользователь не подтвердил телефон, удалить данные о телефоне и создать нового
            if($phone->confirm===self::STATUS_NOT_CONFIRMED){
                $phone->delete();
                return self::save($provider, $social_profile);

            }

            $user= $phone->user;
            self::save($user->id, $provider->id, $social_profile->id);
            return $user;
        }*/


// пользователь не найден, создать нового пользователя
        $user  = self::save($provider, $social_profile);
        return $user;

    }


//Сохранить ключ пользователя и провайдера с соц. сети
        static protected function saveProvider($user_id, $provider_id, $social_profile_id)
        {
            Social::create([
                'user_id' => $user_id,
                'provider_id' => $provider_id,
                'social_id' => $social_profile_id
            ]);
        }

//получить данные о профиле пользователя
        static public function getDataProfile(User $user)
        {
            if(!$user){
                throw new \Exception('User not find');
            }

            $emails = array_column($user->emails->toArray(),'email');
            $phones = array_column($user->phones->toArray(),'number');

            $data = [
                'id'=>$user->id,
                'name'=>$user->name. ' '. $user->surname,
                'avatar'=>$user->avatar?ImageService::getPath($user->avatar->name, $user->id):null,
                'bg_image'=>$user->background ? ImageService::getPath($user->background->name, $user->id):null,
                'role'=>$user->role?$user->role->name:null,
                'emails'=>$emails,
                'phones'=>$phones,

            ];

            return $data;
        }

        static protected function save($provider, $social_profile)
        {

            $avatar = new ImageService($social_profile->avatar);
            $image = Image::create([
                'name' => $avatar->getName()
            ]);

            $user = User::create([
                'name' => $social_profile->name,
                'surname' => $social_profile->surname,
                'bdate' => $social_profile->bdate,
                'avatar_id' => $image->id
            ]);

            Social::create([
                'user_id' => $user->id,
                'provider_id' => $provider->id,
                'social_id' => $social_profile->id
            ]);


            if ($social_profile->email) {
                Email::create([
                    'user_id' => $user->id,
                    'email' => $social_profile->email,
                    'confirm'=>UserService::STATUS_CONFIRMED
                ]);
            }

            if ($social_profile->phone) {
                Phone::create([
                    'user_id' => $user->id,
                    'number' => $social_profile->email,
                    'confirm'=>UserService::STATUS_CONFIRMED
                ]);
            }

            $avatar->save($user->id);

            return $user;
        }
}