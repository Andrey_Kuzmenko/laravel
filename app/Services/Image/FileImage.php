<?php
/**
 * Created by PhpStorm.
 * User: avrora
 * Date: 5/23/17
 * Time: 1:09 PM
 */

namespace App\Services\Image;
use Storage;

class FileImage extends Image
{
    protected $file;

    public function __construct($file)
    {
        $this->file = $file;
        parent::__construct();
    }

    public function save($path, $disk)
    {
        // TODO: Implement save() method.
        $path = self::transformPath($path);
        if($this->file_name){
           Storage::disk($disk)->put($path .'/'.$this->file_name, $this->file);

        }

        return $this->file_name;
    }
}