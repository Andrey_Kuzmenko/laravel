<?php
/**
 * Created by PhpStorm.
 * User: avrora
 * Date: 5/23/17
 * Time: 12:49 PM
 */

namespace App\Services\Image;

abstract class Image
{

    protected $file_name;


    public function __construct()
    {

        $this->file_name = str_random(40).'.jpg';

    }


    abstract public function save($path, $disk);


    public function getFileName()
    {
        return $this->file_name;
    }

    // шифрование путей
    static public function transformPath($path)
    {
        return md5($path);
    }

}