<?php
/**
 * Created by PhpStorm.
 * User: avrora
 * Date: 5/23/17
 * Time: 1:09 PM
 */

namespace App\Services\Image;
use GuzzleHttp\Client;
use Storage;


class LinkImage extends Image
{
    protected $link;

    public function __construct($link)
    {
        $this->link = $link;
        parent::__construct();

    }
    public function save($path, $disk)
    {
        // TODO: Implement save() method.
        $path = self::transformPath($path);
        if($this->file_name){
            $res = $this->getResourceImage();
            Storage::disk($disk)->put($path .'/'.$this->file_name, $res);

        }

        return $this->file_name;
    }

    protected function getResourceImage()
    {

        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => $this->link,
            // You can set any number of default request options.
            'timeout' => 2.0,
        ]);

        $response = $client->request('GET');

        $res = $response->getBody()->getContents();

        return $res;
    }
}