<?php
/**
 * Created by PhpStorm.
 * User: avrora
 * Date: 5/23/17
 * Time: 1:09 PM
 */

namespace App\Services\Image;
use Storage;

class UploadedFileImage extends Image
{
    protected $file;

    public function __construct($file)
    {
        $this->file = $file;
        $this->file_name = $this->file->hashName();
    }

    public function save($path, $disk)
    {
        // TODO: Implement save() method.

        $path = self::transformPath($path);

        Storage::disk($disk)->put($path, $this->file);

        return $this->getFileName();
    }

    public function getFileName()
    {
        return $this->file_name;
    }
}