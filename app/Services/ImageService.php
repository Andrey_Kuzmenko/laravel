<?php
/**
 * Created by PhpStorm.
 * User: avrora
 * Date: 5/10/17
 * Time: 1:26 PM
 */

namespace App\Services;

use App\Services\Image\LinkImage;
use App\Services\Image\FileImage;
use App\Services\Image\UploadedFileImage;
use Storage;
use Illuminate\Http\UploadedFile;

class ImageService
{
    private $img;

    public function __construct($file)
    {
        if($file instanceof UploadedFile)
        {
            $this->img = new UploadedFileImage($file);
            return;
        }
        if(preg_match('~http[s]?://~',$file)){
            $this->img = new LinkImage($file);
        }else{
            $this->img = new FileImage($file);
        }


    }

    public function save($path, $disk='img')
    {
        $this->img->save($path, $disk);
        return $this->img->getFileName();


    }

    public function getName()
    {
        return $this->img->getFileName();
    }


    public function getLink($file=null)
    {
            return $this->img->getLink($file);
    }

    static function getPath($file_name, $path, $disk='img')
    {
        return Storage::disk($disk)->url(FileImage::transformPath($path) . '/' . $file_name);
    }

    static function delete($file_name, $path, $disk='img')
    {
        Storage::disk($disk)->delete(FileImage::transformPath($path) . '/' . $file_name);
    }


}