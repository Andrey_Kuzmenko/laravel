<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master_Brand extends Model
{
    protected $table = 'master_brand';
}
