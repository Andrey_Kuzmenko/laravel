@extends('layouts.app')

@section('content')
    <div class="container">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form action="{{route('gallery.store', ['id'=>2])}}" method="post" enctype="multipart/form-data">
                    Select image to upload:
                    {{ csrf_field() }}
                    <input type="file" name="image" id="image">
                    <input type="submit" value="Upload Image" name="submit">
                </form>
            </div>
        </div>
    </div>
@endsection