<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test</title>
</head>
    
<body>
    <ul>
        <li><a href="{{route('social_login', ['provider'=>'vkontakte'])}}">Вконтакте</a></li>
        <li><a href="{{route('social_login', ['provider'=>'facebook'])}}">Facebook</a></li>
        <li><a href="{{route('social_login', ['provider'=>'google'])}}">Google+</a></li>
    </ul>
    
    <h1>Тесты</h1>
    <a href="{{route('test_profile')}}">CurrentUser</a>
</body>
</html>