@extends('layouts.app')
@section('content')
    <div class="container">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form action="TwoFactor" method="post" >
                    Select image to upload:
                    {{ csrf_field() }}
                    <input name="token" id="token">
                    <input type="submit" value="Send" name="submit">
                </form>
            </div>
        </div>
    </div>
@endsection