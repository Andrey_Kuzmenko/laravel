@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add Brand</div>
                <div class="panel-body">
                    <div class="col-md-10 ">
                        <form action="brand" method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-md-4 control-label">Выбери бренд</label>
                                <div class="col-md-6">
                                    <select name="name">
                                        <option value="Бренд 1">Бренд 1</option>
                                        <option value="Бренд 2">Бренд 2</option>
                                        <option value="Бренд 3">Бренд 3</option>
                                        <option value="Бренд 4">Бренд 4</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <input name="id">
                            </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection