@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Add Service</div>
                <div class="panel-body">
            <div class="col-md-10 ">
                <form action="service/1" method="post" >
                    <input name="_method" type="hidden" value="PUT">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name" class="col-md-6 control-label">Name Service</label>

                        <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" >

                        </div>
                    </div>

                    <div class="form-group">
                        <label for="priceMin" class="col-md-6 control-label">Price Min</label>

                        <div class="col-md-6">
                            <input id="priceMin" type="text" class="form-control" name="priceMin" >

                        </div>
                    </div>
                    </br>
                    <div class="form-group">
                        <label for="priceMax" class="col-md-4 control-label">Price Max</label>

                        <div class="col-md-6">
                            <input id="priceMax" type="text" class="form-control" name="priceMax" >

                        </div>
                    </div>
                    <div class="form-group">
                        <label for="time" class="col-md-4 control-label">Time</label>

                        <div class="col-md-6">
                            <input id="time" type="text" class="form-control" name="time" >

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-8 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Add
                            </button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
        </div>
    </div>
@endsection