@extends('layouts.app')

@section('content')
    <div class="container">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <form action="upload" method="post" enctype="multipart/form-data">
                    Select image to upload:
                    {{ csrf_field() }}
                    <select name="id" id ="id">
                        <option value="0">Аватар</option>
                        <option value="1">Фон</option>
                    </select>
                    <input type="file" name="image" id="image">
                    <input type="submit" value="Upload Image" name="submit">
                </form>
            </div>
        </div>
    </div>
@endsection