<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Styles -->
    <link rel="stylesheet" href="<?= asset('node_modules/swiper/dist/css/swiper.css')?>">
    <link rel="stylesheet" href="<?= asset('assets/css/main.css')?>">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300" rel="stylesheet">

    <!-- Angular libs -->
    <script src="<?= asset('node_modules/angular/angular.min.js')?>" defer></script>
    <script src="<?= asset('node_modules/@uirouter/angularjs/release/angular-ui-router.js')?>" defer></script>
    <script src="<?= asset('node_modules/angular-route/angular-route.js')?>" defer></script>
    <script src="<?= asset('node_modules/angular-animate/angular-animate.js')?>" defer></script>
    <script src="<?= asset('node_modules/swiper/dist/js/swiper.js')?>" defer></script>
    <script src="<?= asset('node_modules/angular-swiper/dist/angular-swiper.js')?>" defer></script>

    <!-- Built JS-->
    <script src="<?= asset('assets/js/built.js') ?>" defer></script>

    <!-- Google maps -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDnycWatbGyK6ldFqErjFtko1yeMclNUOA&sensor=false" defer></script>

    <title>cruckie</title>
</head>
<body>

    <navigation></navigation>
    <div ui-view="menu"></div>

    <main ui-view>
        <!-- Content -->
    </main>

</body>
</html>
