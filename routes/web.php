<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Андрей ---------------------------------------------------------------------------

Route::get('/link', function () {
    return view('Link');
});

//Auth::routes();
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->middleware('confirm');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/twoFactor', 'Auth\LoginController@TwoFactor');
//Route::get('/TwoFactor', 'Auth\LoginController@TwoFactor');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register')->middleware('loginCheck');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('resetPassword', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::put('updatePassword', 'Auth\ResetPasswordController@reset');
Route::get('/resetToken/{path}', 'Auth\ForgotPasswordController@checkToken');
Route::get('/resetToken', function () {
    return view('formToken');
});
/*Route::get('/resetPassword', function () {
    return view('resetPassword');
});*/
//formToken
Route::post('/resetToken', 'Auth\ForgotPasswordController@checkPhoneToken');

Route::get('/home', 'HomeController@index')->name('home');\
Route::get('/findForm',function () {
        return view('findForm');
    });
Route::get('/find','MasterController@search');
Route::resource('service', 'ServiceController');
Route::post('service/put/{id}', 'ServiceController@updatePost');
Route::resource('category', 'CategoryController');
Route::get('category/{master_id}/{cat_id}','CategoryController@showCurrent');
Route::get('service/one/{id}','ServiceController@showOne');
Route::resource('brand','BrandController');
Route::resource('email','EmailController');
Route::resource('phone','PhoneController');

Route::put('profile/{id}','ProfileController@update');

/*VerificationController Begin*/
Route::get('/addNewToken', 'VerificationController@addNewToken');
/*Mobile*/
Route::get('/sendToken', 'VerificationController@sendToken');
Route::get('/addNewMobileToken', 'VerificationController@addNewMobileToken');
/*Mobile END*/
Route::get('/role/{role}', 'VerificationController@chooseRole');
Route::get('/verification/{path}', 'VerificationController@verification')/*->middleware('roleCheck')*/;
Route::post('/verification', 'VerificationController@verificationPhone');
Route::get('/verification',function () {
    return view('verification');
});
/*VerificationController End*/


//Славик------------------------------------------------------------------------------

//Главная страница---------------------------------------------------------------------
Route::get('/', function () {
    return view('index');
})->name('main');

//Регистрация(аунтификация) через соц. сети------------------------------------------
Route::get('/login/{provider}', ['uses'=>'SocialController@login','as'=>'social_login']);

Route::get('/callback_login/{provider}', ['uses'=>'SocialController@callback','as'=>'social_callback']);



Route::group(['prefix'=>'user'], function(){

    //Получить профиль пользователя----------------------------------------------
    Route::get('current', ['uses'=>'ProfileController@index','as'=>'profile']);
    Route::get('/{id}', ['uses'=>'ProfileController@index', 'as'=>'prof']);
    //Включить(отключить) двухфакторную верификацию
    Route::put('/user/{id}/factor','ProfileController@factor');
});

// Мастер,
    Route::get('master/{id}/gallery', ['uses'=>'GalleryController@index', 'as'=>'gallery.index'])->middleware('auth');

Route::group(['prefix'=>'master/{id}', 'middleware' => 'master'], function(){

    Route::match(['PUT'],'gallery',['uses'=>'GalleryController@update', 'as'=>'gallery.update']);
    Route::resource('gallery','GalleryController',[
        'only'=>['store','destroy']
    ]);
});




//Контролер RESTful Address---

Route::resource('user.address', 'AddressController');




//Тестирование, чтобы не мешать другим.-------------------------------------
Route::get('/user/test', ['uses'=>'ProfileController@test','as'=>'test_profile']);

//Основной тестовый контроллер (удалать в prod)---------------------------------------
Route::get('/test', 'TestController@index')->name('test');
Route::get('/t', 'TestController@test')->name('t');




